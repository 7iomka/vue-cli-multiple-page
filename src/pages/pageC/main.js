import Vue from 'vue'
import App from './app'
import CButton from '../../components/button/button';
import router from './router/index'
import 'babel-polyfill'
import '@/utils/rem'
import '@/utils/fixed'
import '../../assets/styles/index.scss'

Vue.config.productionTip = false

new Vue({
  router,
  // render: h => h(App),
  components: {
    [App.name]: App,
    [CButton.name]: CButton,
  },
}).$mount('#app')

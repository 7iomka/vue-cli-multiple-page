import Vue from 'vue'
import App from './app'
import CButton from '../../components/button/button';

import 'babel-polyfill'
import '@/utils/rem'
import '../../assets/styles/index.scss'

Vue.config.productionTip = false

new Vue({
  // render: h => h(App),
  components: {
    [App.name]: App,
    [CButton.name]: CButton,

  },
}).$mount('#app')

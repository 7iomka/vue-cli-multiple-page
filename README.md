### vue-cli-multiple-page

This project is a multi-page environment, and the demo directory is a template directory, which can be copied to pages as the basic environment for module development. Whether or not vue-router is required depends on the module.

**Preview**

http://localhost:8080/[pageName]

----

## Project setup (use yarn or npm)
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

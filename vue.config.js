/* eslint-disable no-console */
const path = require('path')
const glob = require('glob')
const colors = require('colors-console')
const pagesStorage = {};

function getPages() {
  glob.sync('./src/pages/**/main.js').forEach(function(pageUrl) {
    const pageCode = pageUrl.split('./src/pages/')[1].split('/main.js')[0]
    if (pagesStorage[pageCode]) {
      console.error(colors('red', `already exist ${pageCode}.\n`))
      process.exit(1)
    }
    pagesStorage[pageCode] = {
      entry: pageUrl, 
      template: `./public/${pageCode}.html`, 
      // template: './public/index.html', 
      filename: `${pageCode}.html`,
    }
  })
  return pagesStorage
}

const pages = getPages();
const indexPageWithExt = `${Object.keys(pages)[0]}.html`;

module.exports = {
  publicPath: "./",
  css: {
    loaderOptions: {
      sass: {
        sassOptions: {
          includePaths: [path.join(__dirname, "src/assets/styles")]
        }
      }
    }
  },
  pages,
  filenameHashing: false,
  runtimeCompiler: true, // render <someComponent /> in template
  devServer: {
    index: indexPageWithExt,
    historyApiFallback: {
      rewrites: [
        // {
        //   from: /^\/pageA\/?.*/,
        //   to: path.posix.join("/", "pageA.html")
        // },
        // {
        //   from: /^\/pageB\/?.*/,
        //   to: path.posix.join("/", "pageB.html")
        // },
        // {
        //   from: /^\/pageC\/?.*/,
        //   to: path.posix.join("/", "pageC.html")
        // },
        ...Object.keys(pages).map(pageCode => {
          
          const re = new RegExp(`/\\b${pageCode}\\b/?(.*)?$`, "i");
          return {
            from: re,
            to: path.posix.join("/", `${pageCode}.html`)
          };
        }),
        // { from: /./, to: "404.html" }
        // { from: /./, to: path.posix.join("/", 'index.html') }
      ]
    }
  }
  // {
  //   pageA: {
  //     entry: 'src/pages/pageA/index.js',
  //     template: 'public/index.html',
  //     filename: 'pageA.html',
  //   },
  //   pageB: {
  //     entry: 'src/pages/pageB/index.js',
  //     template: 'public/index.html',
  //     filename: 'pageB.html',
  //   }
  // }
};
